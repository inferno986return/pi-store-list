# pi-store-list
An attempt at an exhaustive list of apps and games there were on the Pi Store from 2012-2016. Using information from images, blogs and the Wayback Machine. This project's goal is to document the history of the Pi Store and hopefully resuscitate a few projects.

## Background
The Pi Store was an app store not unlike the Apple App Store or Google Play for the Raspberry Pi and was launched on [17 December, 2012](https://www.raspberrypi.org/blog/introducing-the-pi-store/) through a collaboration between the Raspberry Pi Foundation and the now-defunct IndieCity. It hosted many different projects, most of them free and some costing a small amount of money usually between £1.99 to £4.99 GBP.

The project was forced to close under an apparent EU ruling that disallowed a not-for-profit such as the Raspberry Pi Foundation from operating a for-profit storefront, as such the project was closed down and is now considered a mere piece of the Raspberry Pi's history.

## License
This project is licensed under CC-BY-SA 3.0.

### Remix EULA
The Remix EULA was a license specific to the Pi Store that allowed alteration of other developer's projects providing they were also uploaded to the Pi Store under the same license. The developer had to be credited too.

## Apps
| Name and Official URL | Price | Developer    | Description | License | Active |
|:---------------------:|:------:|:-----:|:-----------:|:-------:|:------:|
| [Asterisk](http://www.raspberry-asterisk.org/) | Free | Digium, Inc. |             | GPLv2   | Yes    |
| Audio Controller | | | | | |
| [BarracudaDrive (now FuguHub)](http://fuguhub.com/RaspberryPi.lsp) | Free | Real Time Logic LLC | | Proprietary | Yes |
| [Calculator++](http://store.raspberrypi.com/projects/calculator) | Free | TironisSoftWorks | A simple calculator made in C++ and GTK. | RemixEULA | No |
| Calorie Counter | | | | | | 
| Cricket Scores LIVE | | | | | |
| [LibreOffice](https://www.libreoffice.org/) | Free | The Document Foundation | | MPLv2 | Yes |
| [OMX Player](https://github.com/huceke/omxplayer) | Free | Edgar Hucek | | GPLv2 | |
| PiLauncher | | | | | |
| Scamp AV Player | | | | | |
| USB over IP | | | | | |
| WebIOPi | | | | | |

## Games
* The Abandoned Farmhouse Adventure
* Iridium Rising
* King's Treasure
* Lunar Panda
* Storm in a Teacup
* The Little Crane That Could
* RPi-Board
* Mushroom Bounce!
* Dr Bulbaceous - Puzzle Solver
* King's Treasure
* Minesweeper CLI
* Battleship (CLI)
* Multisnake
* Solitaire
* Noughts and Crosses
* Mini-sub game
* Pong Singleplayer
* [Chocolate Doom](https://www.chocolate-doom.org/wiki/index.php/Chocolate_Doom)
* Hunted
* [POWDER](http://www.zincland.com/powder/)
* OpenArena
* [OpenTTD](https://www.openttd.org/en/)
* [FreeCiv](http://www.freeciv.org/)
* Pi Cars Software Tools App
* Crazy Ping
* DMazeD
* Martian Invaders
* [Quip - Blow Your Friends Away](https://bitbucket.org/xixs/leedshack/src)

## Tutorials
| Name and Official URL | Price | Developer | Description | License | Active |
|:---------------------:|:-----:|:---------:|:-----------:|:-------:|:------:|
| mOway + Scratch Guide | Free  |  | |   |   |
| Python SQL Database | Free  |  | |   |   |
| Raspberry Invaders | Free | Blitz Games | 17 lessons in Python to teach the beginner how to make a space invaders-style game. Complete with instructions, graphics and audio | Remix EULA | No  |
| Teamspeak Server On Raspberry Pi | Free | [DoseOfTech](http://www.doseoftech.co.uk) | A tutorial showing you how to set up a Teamspeak server on the Raspberry Pi. Note: Requires Windows PC | | |

## Emulators
* Atari800
* FastDOSBox
* rpix x86

## Dev Tools
* 9VA Pi
* Audio Pack
* Chimera Engine
* Code::Blocks
* Grafx2
* HUD Sprite Pack
* Py3D
* Schism Tracker

## Media
The Media section of the Pi Store only had issues of the Raspberry Pi's official magazine, the (MagPi)[].
